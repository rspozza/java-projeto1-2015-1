package utfpr.ct.dainf.if62c.projeto;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;
    
    public Ponto(){
        this.x=this.y=this.z=0;
    }
    
    public Ponto(double x, double y, double z){
        this.x=x;
        this.y=y;
        this.z=z;        
    }
    
    public double dist(Ponto p){
        return Math.sqrt(Math.pow(p.getX()-this.getX(),2)+Math.pow(p.getY()-this.getY(),2)+Math.pow(p.getZ()-this.getZ(),2));        
    }
    
    @Override
    public String toString(){
        return this.getNome()+"("+String.format("%f",this.x)+","+String.format("%f",this.y)+","+String.format("%f",this.z)+")";
    }
    
    @Override
    public boolean equals(Object o){
        if(this == o)
            return true;
        if (o == null)
            return false;
        if (o instanceof Ponto)
            return false;
        Ponto p=(Ponto) o;
        if(this.x==p.getX() && this.y==p.getY() && this.z==p.getZ())
            return true;
        else return false;        
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the z
     */
    public double getZ() {
        return z;
    }

    /**
     * @param z the z to set
     */
    public void setZ(double z) {
        this.z = z;
    }

}
