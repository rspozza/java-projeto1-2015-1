/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author pozza
 */
public class PontoXZ extends Ponto2D{
    
    public PontoXZ() {
        super();
    }

    public PontoXZ(double x, double y, double z) {
        super(x, 0, z);
    }

    public String toString() {
        return this.getNome() + "(" + String.format("%f", this.getX()) + "," + String.format("%f", this.getZ()) + ")";
    }
    
}
