/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

import java.util.ArrayList;

/**
 *
 * @author pozza
 */
public class Poligonal<T> {
    
    T[] vertices;
    
    public Poligonal(T[] vertices) throws Exception{
        if(vertices.length<1)
            throw new Exception("Poligonal deve ter ao menos 2 vértices");
        this.vertices=vertices;
    }
    
    public int getN(){
        return vertices.length;
    }
    
    public T get(int n){
        if(n>this.getN() || n<this.getN())
            return null;
        return this.vertices[n];        
    } 
    
    public void set(int n, T vertice){
        if((n>-1) && (n<this.getN()+1))
            this.vertices[n]=vertice;
    }
    
    public double getComprimento(){
        Ponto2D p1=(Ponto2D) this.get(0);
        Ponto2D p2=(Ponto2D) this.get(vertices.length-1);
        return p1.dist(p2);
    }
    
}
