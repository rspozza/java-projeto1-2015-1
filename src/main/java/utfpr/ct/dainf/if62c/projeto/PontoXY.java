/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author pozza
 */
public class PontoXY extends Ponto2D {

    public PontoXY() {
        super();
    }

    public PontoXY(double x, double y, double z) {
        super(x, y, 0);
    }

    public String toString() {
        return this.getNome() + "(" + String.format("%f", this.getX()) + "," + String.format("%f", this.getY()) + ")";
    }

}
